import java.util.LinkedList;

/*
http://enos.itcollege.ee/~jpoial/algoritmid/magpraktnew.html
http://enos.itcollege.ee/~jpoial/algoritmid/
https://echo360.e-ope.ee/ess/portal/section/c9b3dec6-ad52-4658-91f1-6e04147d238d
https://bitbucket.org/i231

Lauri BitBucket 

Implement an abstract data type "Stack of long integers" (LIFO) using linkedlists. 
String representation of a stack (provided by toString method) must be ordered from bottom to top (tos is the last element). 
List of compulsory operations: 
Constructor for a new stack: LongStack() 
Copy of the stack: Object clone() 
Check whether the stack is empty: boolean stEmpty() 
Adding an element to the stack: void push (long a) 
Removing an element from the stack: long pop() 
Arithmetic operation s ( + - * / ) between two topmost elements of the stack (result is left on top): void op (String s) 
Reading the top without removing it: long tos() 
Check whether two stacks are equal: boolean equals (Object o) 
Conversion of the stack to string (top last): String toString() 

Write a method 
public static long interpret (String pol) 
to calculate the value of an arithmetic expression pol in RPN (Reverse Polish Notation) using this stack type. 
Expression is a string which contains long integers (including negative and multi-digit numbers) and arithmetic 
operations + - * / separated by whitespace symbols. The result must be a long integer value of the expression or 
throwing a RuntimeException in case the expression is not correct. Expression is not correct if it contains 
illegal symbols, leaves redundant elements on top of stack or causes stack underflow. 
Example. LongStack.interpret ("2 15 -") should return -13 . 

Realiseerida abstraktne andmetüüp "pikkade täisarvude magasin" (LIFO) ahela (linkedlist) abil. Magasini sõnena 
esitamisel olgu tipp lõpus (meetod toString() väljastab elemendid põhja poolt tipu poole). 
Operatsioonide loetelu: 
uue magasini konstruktor: LongStack() 
koopia loomine: Object clone() 
kontroll, kas magasin on tühi: boolean stEmpty() 
magasini elemendi lisamine: void push (long a) 
magasinist elemendi võtmine: long pop() 
aritmeetikatehe s ( + - * / ) magasini kahe pealmise elemendi vahel (tulemus pannakse uueks tipuks): void op (String s) 
tipu lugemine eemaldamiseta: long tos() 
kahe magasini võrdsuse kindlakstegemine: boolean equals (Object o) 
teisendus sõneks (tipp lõpus): String toString() 

Koostada meetod signatuuriga 
public static long interpret (String pol) 
aritmeetilise avaldise pööratud poola kuju (sulgudeta postfikskuju, Reverse Polish Notation) pol interpreteerimiseks 
(väljaarvutamiseks) eelpool defineeritud pikkade täisarvude magasini abil. Avaldis on antud stringina, mis võib 
sisaldada pikki täisarve (s.h. negatiivseid ja mitmekohalisi) ning tehtemärke + - * / , mis on eraldatud tühikutega (whitespace). 
Tulemuseks peab olema avaldise väärtus pika täisarvuna või erindi (RuntimeException) tekitamine, kui avaldis ei ole korrektne. 
Korrektne ei ole, kui avaldises esineb lubamatuid sümboleid, kui avaldis jätab magasini üleliigseid elemente või kasutab magasinist liiga palju elemente. 
Näit. LongStack.interpret ("2 15 -") peaks tagastama väärtuse -13 .
*/

// enos.itcollege.ee/~jpoial/algoritmid/adt.html

/*
algoritmid - ArrayList, LinkedList parem!!!! /java/i200loeng7.html

vabane neist:
private int () mag;
/tipu indeks.
private int SP;

tee ise et oleks linkedlist… sama signatuuriga asjad, et main method töötaks ilma muutusteta meie versioonist. 
Sisekuju pole massiiv ja SP vaid linkedlist, mille viimane element on top… pop = .removelast ja push = .add

linkedlistiga tagastame ka stackobjecti
*/

public class LongStack {

	public static void main(String[] argum) {

//		String s = "35 35 35 / + +";
//	    System.out.println(LongStack.interpret (s)); 
		
		
	} // main

	private LinkedList<Long> list;

	public LongStack() { // constructor, peab tegema uue magasini
		list = new LinkedList<Long>();
	} // LongStack

	
	@Override
	public Object clone() throws CloneNotSupportedException { // peab tegema klooni
		LongStack clone = new LongStack();
		for (int i = list.size() - 1; i >= 0; i--) {
			clone.push(list.get(i));
		}
		return clone; // TODO!!! Your code here!
	} // clone

	public boolean stEmpty() { // kas on tühi, kas on võimalik võtta või ei
		return (list != null);
	} // stEmpty

	
	public void push(long a) { // peab lisama elemendi a magasini tipuks
		list.addFirst(a);
	} // push

	public long pop() { // pop peab tagastama ja ära võtma sealt tipust arvu
		return list.removeFirst();
	} // pop

   public void op (String s) {  //  korjab ära magasini pealt 2 pealmist, teeb nende vahel nõutud tehte s ja paneb pushiga tehte tulemuse magasini tippu. 
	   							//  Teine operand korjatakse ära, muutujasse op2, sama esimesega... sis vaadatake tehe, tehakse vastav tehe, 
	   							//  tulemus pushiga magasini tippu.
		long second = list.pop();
		long first = list.pop();
		long result;
		if (s.equals("+")) {
			result = first + second;
			list.push(result);
		}
		if (s.equals("-")) {
			result = first - second;
			list.push(result);
		}
		if (s.equals("*")) {
			result = first * second;
			list.push(result);
		}
		if (s.equals("/")) {
			result = first / second;
			list.push(result);
		}
	} // op  

   
	public long tos() { // loeb pealt ilma magasini muutmata
		return list.getFirst(); // TODO!!! Your code here!
	} // tos

	
	@Override
	public boolean equals(Object o) { // ütleb millal kaks on võrdsed
		// NB! järgnev baseerub adt.html'is leiduval koodil!

		if (((LongStack) o).list.size() != list.size()) {
			return false;
		}
		for (int i = 0; i < list.size(); i++) {
			if (((LongStack) o).list.get(i) != list.get(i)) {
				return false;
			}
			return true;
		}
		return true;
	} // equals
	
	
	@Override
	public String toString() { // teha niipidi, et top/tipp oleks viimane! LinkedListi push ja pop puhul tuleb toString valet pidi.
							   // Tuleks teha tagurpidi tsükkel siis! Kui aga push=.add ja pop=.removelast siis on kõik ok, ehk top lõpus toStringis.
		String string = "";
		if (list.isEmpty() == true) {
			return string;
		}
		for (int i = list.size(); i > 0; i--) {
			string = string + list.get(i - 1);
		}
		return string;
	} // toString

/*
 * õnnetu. Peab oskama suvalisest poolakuju avaldisest pop välja arvutada avaldise väärtus ja tagastada see arvuna. 
 * Õnnetus on see, et "kui ei ole korrektne avaldis" 3 olukorda vaja kontrollida ja siis visata runtime exception. 
 * a) tundmatu märk 
 * b) kui avaldis tekitab alatäitumist (2-) 
 * c) 235 ja pole tehteid ehk jääb järgi rohkem kui 1 element. visata runtime exception. eraldi kontrolli,
 * et oleks lõpus 1 arv alles popiga ja kontrollida, kas jäi tühjaks. viimati välja võetud arv returniga avaldise väärtusena tagastada popiga 
 */
	public static long interpret(String pol) {
		String s;
		long value;
		LongStack m = new LongStack();
		String[] split = pol.split("\\s+");
		
		if (split.length < 3 && split[(split.length) - 1].equals("\\p{Alnum}")) {  // kui alla 3 elemendi ja viimane element on number, siis viska RuntimeException
			throw new RuntimeException("Anna vähemalt 3 sisendit: arv, arv, tehe // Sisend = " + pol);
		}
		
		for (int i = 0; i < split.length; i++) {
			if (split[i].equals("+") || split[i].equals("-") || split[i].equals("*") || split[i].equals("/")) {
				s = split[i];
				m.op(s);
			} 
			else if (split[i].equals("")) { // do nothing
			}
//			else if () {
//				
//			}
			else {
				value = Long.parseLong(split[i], 10);
				m.push(value);
			}
		}
		if (m.list.size() > 1) {
			throw new RuntimeException("Lõplik magasin sisaldab üle ühe väärtuse. Sisesta vähem väärtusi või rohkem tehteid! Sisend = " + pol);
		} else {
			return Long.parseLong(m.toString());
		}
	} // interpret

}
